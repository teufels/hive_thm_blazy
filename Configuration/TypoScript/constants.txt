## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder

########################################
## INCLUDES FOR STAGING && PRODUCTION ##
########################################
<INCLUDE_TYPOSCRIPT: source="DIR:EXT:hive_thm_blazy/Configuration/TypoScript/Constants/Production" extensions="txt">

##############################
## OVERRIDE FOR DEVELOPMENT ##
##############################
[request.getNormalizedParams() && like(request.getNormalizedParams().getHttpHost(), "/development.*/")]
<INCLUDE_TYPOSCRIPT: source="DIR:EXT:hive_thm_blazy/Configuration/TypoScript/Constants/Development" extensions="txt">
[END]